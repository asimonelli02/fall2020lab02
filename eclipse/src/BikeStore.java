//Antonio Simonelli 1736100
import lab02.eclipse.Bicycle;

public class BikeStore {

	public static void main(String[] args) {
		Bicycle[] bike = new Bicycle[4];
		bike[0] = new Bicycle("Specialized", 21, 40);
		bike[1] = new Bicycle("Specialized", 32, 50);
		bike[2] = new Bicycle("Specialized", 43, 60);
		bike[3] = new Bicycle("Specialized", 54, 70);

		for (int i = 0; i < bike.length; i++) {
			System.out.println(bike[i]);
		}

	}

}
